#!/usr/bin/env python
import sys

import RPi.GPIO as gpio
import time
speedlpin = 12
fwlpin = 22
bwlpin = 27
speedrpin = 13
fwrpin = 24
bwrpin = 23
speed = 70
straight_line_correction = 1.1
gpio.setmode(gpio.BCM)
gpio.setwarnings(False)
forward = 'w'
backward = 'x'
left = 'a'
right = 'd'
stop = 's'
leftish = 'q'
rightish = 'e'
faster = 't'
slower = 'g'

gpio.setup(speedrpin, gpio.OUT)
gpio.setup(speedlpin, gpio.OUT)
speedl = gpio.PWM(speedlpin, 100)
speedr = gpio.PWM(speedrpin, 100)

print('pwm setup done')

gpio.setup(fwlpin, gpio.OUT) #fwl
gpio.setup(27, gpio.OUT) #bwl
gpio.setup(23, gpio.OUT) #fwr
gpio.setup(24, gpio.OUT) #bwr

speedl.start(speed)
speedr.start(speed)
print('pins setup done')
print('azerty? (y/n)')
answer = input()
if answer == 'y':
    forward = 'z'
    leftish = 'a'
    left = 'q'
print('press p to quit')
def move(action):
    #refer as global var to change it in function
    global speed
    #set motor speeds to regular again if previous action was turning
    speedl.ChangeDutyCycle(speed)
    speedr.ChangeDutyCycle(speed * straight_line_correction)
    
    #go a diraction based on input
    if action == forward:
        gpio.output(bwlpin, gpio.LOW)
        gpio.output(fwlpin, gpio.HIGH)
        gpio.output(bwrpin, gpio.LOW)
        gpio.output(fwrpin, gpio.HIGH)
    elif action == backward:
        gpio.output(fwlpin, gpio.LOW)
        gpio.output(bwlpin, gpio.HIGH)
        gpio.output(fwrpin, gpio.LOW)
        gpio.output(bwrpin, gpio.HIGH)
    elif action == stop:
        gpio.output(bwlpin, gpio.LOW)
        gpio.output(fwlpin, gpio.LOW)
        gpio.output(bwrpin, gpio.LOW)
        gpio.output(fwrpin, gpio.LOW)
    elif action == left:
        gpio.output(bwlpin, gpio.LOW)
        gpio.output(fwlpin, gpio.LOW)
        gpio.output(bwrpin, gpio.LOW)
        gpio.output(fwrpin, gpio.HIGH)
    elif action == right:
        gpio.output(bwlpin, gpio.LOW)
        gpio.output(fwlpin, gpio.HIGH)
        gpio.output(bwrpin, gpio.LOW)
        gpio.output(fwrpin, gpio.LOW)
    elif action == faster:
        print('current speed: ' + str(speed))
        if speed <= 90:
            speed += 10
            speedl.ChangeDutyCycle(speed)
            speedr.ChangeDutyCycle(speed * straight_line_correction)
            print('new speed: ' + str(speed))
    elif action == slower:
        print('current speed: ' + str(speed))
        if speed >= 30:
            speed -= 10
            speedl.ChangeDutyCycle(speed)
            speedr.ChangeDutyCycle(speed * straight_line_correction)
            print('new speed: ' + str(speed))
    elif action == leftish:
        gpio.output(bwlpin, gpio.LOW)
        gpio.output(fwlpin, gpio.HIGH)
        gpio.output(bwrpin, gpio.LOW)
        gpio.output(fwrpin, gpio.HIGH)
        speedl.ChangeDutyCycle(speed/2)
    elif action == rightish:
        gpio.output(bwlpin, gpio.LOW)
        gpio.output(fwlpin, gpio.HIGH)
        gpio.output(bwrpin, gpio.LOW)
        gpio.output(fwrpin, gpio.HIGH)
        speedr.ChangeDutyCycle(speed/2 * straight_line_correction)
    #Apply speedcorrection factor to right wheel
        
    
def main():
    getch = _Getch()
    while True:
        choice = getch()
        if choice == 'p':
            break
        move(choice)

    speedl.stop()
    speedr.stop()
    print('stopped speed pin')
    gpio.cleanup()
    print('cleanup done, stopping programme')



class _Getch:
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()
        
    def __call__(self):
        return self.impl()
    
class _GetchUnix:
    def __init__(self):
        import tty, sys
    
    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

class _GetchWindows:
    def __init__(self):
        import msvcrt
    
    def __call__(self):
        import msvcrt
        return msvcrt.getch()
        
if __name__ == "__main__":
    main()

