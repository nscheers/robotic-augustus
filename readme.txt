Project Robotics Nicolas Scheers

Om project te runnen: SSH op pi
3 terminals:
roscore    //  Roscore nodig om nodes te laten runnen
rosrun robot drive.py  //  drive is de aansturing van de robot, deze node luistert naar berichten op de chatter topic
rosrun robot camera.py  //  de camera node diet het denkwerk: het leest de camera uit en berekent aan de hand van
                               deze data een twist message en publisht deze op de chatter topic

optioneel:
rosrun keys.py  //  een node die toelaat om de robot te besturen met een toetsenbord
rosrun cameraPub.py  //  Hetzelfde als camera, maar met output van de frames