#!/usr/bin/env python
import sys

import time
from geometry_msgs.msg import Twist
import rospy

pub = rospy.Publisher('chatter', Twist, queue_size=1)
rospy.init_node('keys')

speed = 70


forward = 'w'
backward = 'x'
left = 'a'
right = 'd'
stop = 's'
leftish = 'q'
rightish = 'e'
bwleftish = 'z'
bwrightish = 'c'
faster = 't'
slower = 'g'

print('azerty? (y/n)')
answer = sys.stdin.read(1)
if answer == 'y':
    forward = 'z'
    leftish = 'a'
    left = 'q'
    bwleftish = 'z'
print('press p to quit')


def move(action):
    # refer as global var to change it in function
    global speed
    msg = Twist()
    # go a direction based on input
    if action == forward:
        msg.linear.x = speed
        msg.angular.z = 90
    elif action == backward:
        msg.linear.x = -speed
        msg.angular.z = 90
    elif action == stop:
        msg.linear.x = 0
        msg.angular.z = 90
    elif action == left:
        msg.linear.x = speed
        msg.angular.z = 0
    elif action == right:
        msg.linear.x = speed
        msg.angular.z = 180
    elif action == faster:
        print('current speed: ' + str(speed))
        if speed <= 90:
            speed += 10

            print('new speed: ' + str(speed))
    elif action == slower:
        print('current speed: ' + str(speed))
        if speed >= 30:
            speed -= 10

            print('new speed: ' + str(speed))
    elif action == leftish:
        msg.linear.x = speed
        msg.angular.z = 45
    elif action == rightish:
        msg.linear.x = speed
        msg.angular.z = 135
    elif action == bwleftish:
        msg.linear.x = -speed
        msg.angular.z = 45
    elif action == bwrightish:
        msg.linear.x = -speed
        msg.angular.z = 135
    pub.publish(msg)

def main():
    getch = _Getch()


    while True:
        choice = getch()
        if choice == 'p':
            break
        move(choice)



class _Getch:
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self):
        return self.impl()


class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()


if __name__ == "__main__":
    main()

