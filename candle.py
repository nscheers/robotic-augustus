#!/usr/bin/env python
# simulate candle light
import random, time
import RPi.GPIO as gpio

gpio.setmode(gpio.BCM)
gpio.setup(18, gpio.OUT)
pwm = gpio.PWM(18, 100)
running = True

def brightness():
    return random.randint(5, 100)

def flicker():
    return random.random() / 8

print ("Stop -> CTRL + C")

while running:
    pwm.start(0)
    pwm.ChangeDutyCycle(brightness())
    time.sleep(flicker())


