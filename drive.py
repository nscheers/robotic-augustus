#!/usr/bin/env python
import sys
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import Twist

import RPi.GPIO as gpio
import time
speedlpin = 12
fwlpin = 22
bwlpin = 27
speedrpin = 13
fwrpin = 24
bwrpin = 23
straight_line_correction = .9
turn_boost = 1.2
gpio.setmode(gpio.BCM)
gpio.setwarnings(False)


gpio.setup(speedrpin, gpio.OUT)
gpio.setup(speedlpin, gpio.OUT)
speedl = gpio.PWM(speedlpin, 100)
speedr = gpio.PWM(speedrpin, 100)

print('pwm setup done')

gpio.setup(fwlpin, gpio.OUT) #fwl
gpio.setup(27, gpio.OUT) #bwl
gpio.setup(23, gpio.OUT) #fwr
gpio.setup(24, gpio.OUT) #bwr

speedl.start(30)
speedr.start(30)
print('pins setup done')
t = time.time()
period = 1

def move(forward, angle):
    global t
    # forward between -100 and 100,
    # angle:
    # left = 0-89 deg
    # right = 91-180 deg
    #straight = 90 deg

    #refer as global var to change it in function

    backwards = False

    if (time.time() < t + period or forward == 666):
        print("Path blocked")
        if forward == 666:  # path blocked
            t = time.time()
            print('will resume in %s seconds' % str((t + period) - time.time()))
        forward = 0
        angle = 90

    if (forward > 100): forward = 100

    if (forward < 0):
        forward = abs(forward)
        backwards = True


    #go a diraction based on input
    if not backwards: #forwards
        gpio.output(bwlpin, gpio.LOW)
        gpio.output(fwlpin, gpio.HIGH)
        gpio.output(bwrpin, gpio.LOW)
        gpio.output(fwrpin, gpio.HIGH)
    elif backwards: #backwards
        gpio.output(fwlpin, gpio.LOW)
        gpio.output(bwlpin, gpio.HIGH)
        gpio.output(fwrpin, gpio.LOW)
        gpio.output(bwrpin, gpio.HIGH)
    if forward == 0.0: #stop
        gpio.output(bwlpin, gpio.LOW)
        gpio.output(fwlpin, gpio.LOW)
        gpio.output(bwrpin, gpio.LOW)
        gpio.output(fwrpin, gpio.LOW)



    #work out the angle
    if angle == 90: # forwards
        speedl.ChangeDutyCycle(forward * straight_line_correction)
        speedr.ChangeDutyCycle(forward)
    elif angle < 90: #left
        turn = angle / 90
        speedl.ChangeDutyCycle(forward * straight_line_correction * turn)
        if forward >= 50:
            speedr.ChangeDutyCycle(forward * turn_boost)
        else:
            speedr.ChangeDutyCycle(forward)

    elif angle > 90: #right
        turn = 1 - ((angle - 90) / 90)
        if forward >= 50:
            speedl.ChangeDutyCycle(forward * turn_boost * straight_line_correction)
        else:
            speedl.ChangeDutyCycle(forward * straight_line_correction)
        speedr.ChangeDutyCycle(forward * turn)

def callback(data):
    # forward should be between 0 - 100, angle between 0 and 180
    rospy.loginfo('Speed is %s' % str(data.linear.x))
    rospy.loginfo('Angle is is %s' % str(data.angular.z))
    xval = data.linear.x
    zval = data.angular.z
    move(xval, zval)


def main():
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber('chatter', Twist, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
    speedl.stop()
    speedr.stop()
    print('stopped speed pin')
    gpio.cleanup()
    print('cleanup done, stopping programme')

        
if __name__ == "__main__":
    main()

