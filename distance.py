#!/usr/bin/env python
# Libraries
import RPi.GPIO as GPIO
import time
import rospy
from geometry_msgs.msg import Twist
import cv2

pub = rospy.Publisher('chatter', Twist, queue_size=10)
rospy.init_node('distance')
rate = rospy.Rate(10)  # 10hz

# GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)

# set GPIO Pins
GPIO_TRIGGER = 4
GPIO_ECHO = 17

# set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)


def distance():
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER, True)

    # set Trigger after 0.01ms to LOW
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)

    StartTime = time.time()
    StopTime = time.time()

    # save StartTime
    while GPIO.input(GPIO_ECHO) == 0:
        StartTime = time.time()

    # save time of arrival
    while GPIO.input(GPIO_ECHO) == 1:
        StopTime = time.time()

    # time difference between start and arrival
    TimeElapsed = StopTime - StartTime
    # multiply with the sonic speed (34300 cm/s)
    # and divide by 2, because there and back
    distance = (TimeElapsed * 34300) / 2

    return distance

def main():
        while True:
            dist = distance()
            print("Measured Distance = %.1f cm" % dist)
            if (dist <= 40):
                msg = Twist()
                msg.linear.x = 666
                msg.angular.z = 0
                pub.publish(msg)
            time.sleep(1)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break





if __name__ == "__main__":
    main()