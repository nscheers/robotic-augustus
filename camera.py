#!/usr/bin/env python

import time
import sys
import cv2
import numpy as np
import math
import rospy
import logging
from geometry_msgs.msg import Twist

rospy.init_node('talker', anonymous=True)
pub = rospy.Publisher('chatter', Twist, queue_size=10)
period = 0.12
default_speed = 45
t = time.time()
cap = cv2.VideoCapture(-1)
logging.basicConfig(level=logging.DEBUG)
logging.getLogger().setLevel(logging.DEBUG)

class lane_follower():

    def __init__(self):
        print('Creating a Camera lane detector')
        self.curr_steering_angle = 90
        self.curr_speed = default_speed

    def process_image(self):
        # Lees image frame uit
        ret, frame = cap.read()
        msg = Twist()

        if (ret):
            frame = cv2.resize(frame, (160, 120))
            # warp image
            # rect = np.array([(120, 90), (200, 90), (319, 239), (1, 239)], dtype=float)
            # warped = four_point_transform(frame, rect)
            edges = detect_edges(frame)
            roi = region_of_interest(edges)
            line_segments = detect_line_segments(roi)
            lane_lines = average_slope_intercept(frame, line_segments)

            if len(lane_lines) == 0:
                print('No lane lines detected, nothing to do.')
                cv2.imshow('lane lines', frame)
                if self.curr_speed != 0:
                    self.curr_speed = 0
                    msg = Twist()
                    msg.linear.x = self.curr_speed
                    msg.angular.z = self.curr_steering_angle
                    pub.publish(msg)
                return frame
            else: self.curr_speed = default_speed

            self.curr_steering_angle = compute_steering_angle(frame, lane_lines)

            msg = Twist()
            msg.linear.x = self.curr_speed
            msg.angular.z = self.curr_steering_angle
            pub.publish(msg)
            print('Steering angle is %s' % str(self.curr_steering_angle))
            lane_lines_image = display_lines(frame, lane_lines)
            heading_line_image = display_heading_line(lane_lines_image, self.curr_steering_angle)
            cv2.imshow('lane lines', heading_line_image)

def detect_edges(frame):
    kleur = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    redLow = np.array([160, 100, 100])
    redHigh = np.array([200, 255, 255])
    redMask = cv2.inRange(kleur, redLow, redHigh)
    edges = cv2.Canny(redMask, 200, 400)
    return edges

def region_of_interest(edges):
    height, width = edges.shape
    mask = np.zeros_like(edges)

    # only focus bottom two thirds of the screen
    polygon = np.array([[
        (0, height * 1 / 2),
        (width, height * 1 / 2),
        (width, height),
        (0, height),
    ]], np.int32)

    cv2.fillPoly(mask, polygon, 255)
    cropped_edges = cv2.bitwise_and(edges, mask)
    return cropped_edges

def detect_line_segments(cropped_edges):
    rho = 1  # distance precision in pixel
    angle = np.pi / 180  # angular precision in radian
    min_threshold = 10  # minimal of votes
    line_segments = cv2.HoughLinesP(cropped_edges, rho, angle, min_threshold,
                                    np.array([]), minLineLength=8, maxLineGap=4)

    return line_segments

def average_slope_intercept(frame, line_segments):
    """
    This function combines line segments into one or two lane lines
    If all line slopes are < 0: then we only have detected left lane
    If all line slopes are > 0: then we only have detected right lane
    """
    lane_lines = []
    if line_segments is None:
        print('No line_segment segments detected')
        return lane_lines

    height, width, _ = frame.shape
    left_fit = []
    right_fit = []

    boundary = 1/3
    left_region_boundary = width * (1 - boundary)  # left lane line segment should be on left 2/3 of the screen
    right_region_boundary = width * boundary # right lane line segment should be on left 2/3 of the screen

    for line_segment in line_segments:
        for x1, y1, x2, y2 in line_segment:
            if x1 == x2:
                print('skipping vertical line segment (slope=inf): %s' % line_segment)
                continue
            fit = np.polyfit((x1, x2), (y1, y2), 1)
            slope = fit[0]
            intercept = fit[1]
            if slope < 0:
                if x1 < left_region_boundary and x2 < left_region_boundary:
                    left_fit.append((slope, intercept))
            else:
                if x1 > right_region_boundary and x2 > right_region_boundary:
                    right_fit.append((slope, intercept))

    left_fit_average = np.average(left_fit, axis=0)
    if len(left_fit) > 0:
        lane_lines.append(make_points(frame, left_fit_average))

    right_fit_average = np.average(right_fit, axis=0)
    if len(right_fit) > 0:
        lane_lines.append(make_points(frame, right_fit_average))

    print('lane lines: %s' % lane_lines)  # [[[316, 720, 484, 432]], [[1009, 720, 718, 432]]]

    return lane_lines

def compute_steering_angle(frame, lane_lines):

    if len(lane_lines) == 0:
        print('No lane lines detected, do nothing')
        return -90

    height, width, _ = frame.shape
    if len(lane_lines) == 1:
        print('Only detected one lane line, just follow it. %s' % lane_lines[0])
        x1, _, x2, _ = lane_lines[0][0]
        x_offset = x2 - x1
    else:
        _, _, left_x2, _ = lane_lines[0][0]
        _, _, right_x2, _ = lane_lines[1][0]
        mid = int(width / 2)
        x_offset = (left_x2 + right_x2) / 2 - mid



    # find the steering angle, which is angle between navigation direction to end of center line
    y_offset = int(height / 2)

    angle_to_mid_radian = math.atan2(y_offset , x_offset)  # angle (in radian) to center vertical line
    angle_to_mid_deg = int(angle_to_mid_radian * 180.0 / math.pi)  # angle (in degrees) to center vertical line


    print('X_offset: %s' % x_offset)
    print('Y offset: %s' % y_offset)
    print('Radian angle to mid: %s' % angle_to_mid_radian)
    print('Raw angle: %s' % angle_to_mid_deg)
    return 180 - angle_to_mid_deg




def make_points(frame, line):
    height, width, _ = frame.shape
    slope, intercept = line
    y1 = height  # bottom of the frame
    y2 = int(y1 * 1 / 2)  # make points from middle of the frame down

    # bound the coordinates within the frame
    x1 = max(-width, min(2 * width, int((y1 - intercept) / slope)))
    x2 = max(-width, min(2 * width, int((y2 - intercept) / slope)))
    return [[x1, y1, x2, y2]]


def display_lines(frame, lines, line_color=(0, 255, 0), line_width=2):
    line_image = np.zeros_like(frame)
    if lines is not None:
        for line in lines:
            for x1, y1, x2, y2 in line:
                cv2.line(line_image, (x1, y1), (x2, y2), line_color, line_width)
    line_image = cv2.addWeighted(frame, 0.8, line_image, 1, 1)
    return line_image

def display_heading_line(frame, steering_angle, line_color=(0, 0, 255), line_width=5 ):
    heading_image = np.zeros_like(frame)
    height, width, _ = frame.shape

    # figure out the heading line from steering angle
    # heading line (x1,y1) is always center bottom of the screen
    # (x2, y2) requires a bit of trigonometry

    # Note: the steering angle of:
    # 0-89 degree: turn left
    # 90 degree: going straight
    # 91-180 degree: turn right
    steering_angle_radian = steering_angle / 180.0 * math.pi
    x1 = int(width / 2)
    y1 = height
    x2 = int(x1 - height / 2 / math.tan(steering_angle_radian))
    y2 = int(height / 2)

    cv2.line(heading_image, (x1, y1), (x2, y2), line_color, line_width)
    heading_image = cv2.addWeighted(frame, 0.8, heading_image, 1, 1)

    return heading_image

def main():
    global t

    lf = lane_follower()
    time.sleep(3)
    while True:
        t += period
        lf.process_image()
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        time.sleep(max(0, t - time.time()))
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
